import express, { Request, Response } from "express";
import bodyParser from "body-parser";

import config from "./config";
import { handleMessage } from "./utils";
import * as imap from "./utils/imap";

async function main() {
  await imap.setup();

  const app = express();

  app.get("/authentication", (req: Request, res: Response) => {
    const { account, proto } = req.query;

    if (account === config.imap.accountID) {
      res.json({
        user: config.imap.auth.user,
        pass: config.imap.auth.password,
      });
    } else {
      res.sendStatus(404);
    }
  });

  app.post("/webhook", bodyParser.json(), (req: Request, res: Response) => {
    if (
      req.body.account === config.imap.accountID &&
      req.body.path === "INBOX" &&
      req.body.event == "messageNew"
    ) {
      try {
        const message: imap.Message = req.body.data;
        handleMessage(message);
      } catch (err) {
        console.log(`Error: handleMessage:`, err);
      }
    }
    res.sendStatus(200);
  });

  app.listen(config.port, config.host, () => {
    console.log("Bot is waiting for emails...");
  });
}

main().catch((err) => {
  console.log(`Error: ${err.message}`);
  process.exit(1);
});
