import * as env from "./utils/env";

if (process.env.NODE_ENV === "development") {
  require("dotenv").config();
}

export default {
  baseURL: env.getString("BASE_URL") || "http://localhost:4000",
  host: env.getString("HOST") || "0.0.0.0",
  port: env.getInteger("PORT") || 4000,
  imap: {
    endpoint: env.requireString("IMAP_API") || "http://localhost:3000",
    accountID: env.getString("IMAP_ACCOUNT_ID") || "default",
    host: env.requireString("IMAP_HOST"),
    port: env.requireInteger("IMAP_PORT"),
    tls: env.requireBoolean("IMAP_TLS"),
    auth: {
      user: env.requireString("IMAP_USER"),
      password: env.requireString("IMAP_PASSWORD"),
    },
  },
  filters: {
    ignoreSubjects: env.getRegExp("IGNORE_SUBJECTS"),
  },
  gitlab: {
    endpoint: env.getString("GITLAB_API") || "https://gitlab.com/api",
    token: env.requireString("GITLAB_TOKEN"),
    projectID: env.requireInteger("GITLAB_PROJECT_ID"),
    labels: env.getString("GITLAB_LABELS"),
  },
};
