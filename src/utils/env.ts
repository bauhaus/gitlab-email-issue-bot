export type EnvType = "string" | "integer" | "regexp" | "boolean";

export type EnvObjectType<T> = T extends "string"
  ? string
  : T extends "integer"
  ? number
  : T extends "regexp"
  ? RegExp
  : T extends "boolean"
  ? boolean
  : never;

export function requireEnv<T extends EnvType>(
  key: string,
  type: T = "string" as T
): EnvObjectType<T> {
  const value = getEnv(key, type);
  if (value === undefined) {
    throw new Error(`Environment variable ${key} is required.`);
  }

  return value;
}

export function getEnv<T extends EnvType>(
  key: string,
  type: T = "string" as T
): EnvObjectType<T> | undefined {
  const value = process.env[key];
  if (value === undefined) {
    return;
  }

  switch (type) {
    case "string":
      return value as EnvObjectType<T>;
    case "integer":
      const numValue = new Number(value).valueOf();
      if (!Number.isInteger(numValue)) {
        throw new Error(`Environment variable ${key} must be integer.`);
      }
      return numValue as EnvObjectType<T>;
    case "regexp":
      const regexpValue = new RegExp(value, getEnv(`${key}_FLAGS`) as string);
      return regexpValue as EnvObjectType<T>;
    case "boolean":
      const booleanValue = value === "true" || value === "1";
      return booleanValue as EnvObjectType<T>;
  }
}

export function getString(key: string): string | undefined {
  return getEnv(key, "string");
}

export function requireString(key: string): string {
  return requireEnv(key, "string");
}

export function getInteger(key: string): number | undefined {
  return getEnv(key, "integer");
}

export function requireInteger(key: string): number {
  return requireEnv(key, "integer");
}

export function getRegExp(key: string): RegExp | undefined {
  return getEnv(key, "regexp");
}

export function requireRegExp(key: string): RegExp {
  return requireEnv(key, "regexp");
}

export function getBoolean(key: string): boolean | undefined {
  return getEnv(key, "boolean");
}

export function requireBoolean(key: string): boolean {
  return requireEnv(key, "boolean");
}
