import * as imap from "./imap";
import * as gitlab from "./gitlab";
import config from "../config";

function sanitizeSubject(subject: string) {
  return subject.replace(/^(\s*(fw|fwd|wg|re|aw):\s*)+/i, "");
}

export async function handleMessage(message: imap.Message) {
  if (
    config.filters.ignoreSubjects &&
    config.filters.ignoreSubjects.test(message.subject)
  ) {
    return;
  }

  const attachments = message.attachments || [];

  console.log(
    `[${new Date().toISOString()}] from: ${message.from.address}, subject: ${
      message.subject
    }, attachments: ${attachments.length}`
  );

  const uploads: gitlab.Upload[] = [];
  for (let attachment of attachments) {
    const data = await imap.downloadAttachment(attachment.id);
    const upload = await gitlab.uploadFile({
      data,
      filename: attachment.filename,
      contentType: attachment.contentType,
      knownLength: attachment.encodedSize,
    });

    if (upload) {
      uploads.push(upload);
    }
  }

  let header = `- **From:** ${message.from.name} ${message.from.address}\n`;
  header += `- **Date:** ${message.date}\n`;
  header += "\n";

  let footer = "\n\n";
  for (let upload of uploads) {
    footer += upload.markdown + "\n\n";
  }

  await gitlab.createIssue({
    title: sanitizeSubject(message.subject),
    description:
      header + gitlab.sanitizeDescription(message.text.plain) + footer,
    labels: config.gitlab.labels,
  });

  await imap.deleteMessage(message.id);
}
