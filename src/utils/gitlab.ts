import axios from "axios";
import FormData from "form-data";

import config from "../config";

const api = axios.create({
  baseURL: config.gitlab.endpoint,
  headers: {
    "PRIVATE-TOKEN": config.gitlab.token,
  },
});

interface Issue {
  title: string;
  description: string;
  labels?: string;
}

export async function createIssue(issue: Issue) {
  await api.post(`v4/projects/${config.gitlab.projectID}/issues`, issue);
}

interface File {
  data: Buffer;
  filename: string;
  contentType: string;
  knownLength: number;
}

export interface Upload {
  alt: string;
  url: string;
  full_path: string;
  markdown: string;
}

export async function uploadFile(file: File): Promise<Upload | undefined> {
  const projectID = 24348025;

  const form = new FormData();
  const { data, ...options } = file;
  form.append("file", data, options);

  try {
    const res = await api.post(
      `v4/projects/${config.gitlab.projectID}/uploads`,
      form,
      {
        headers: {
          ...form.getHeaders(),
        },
      }
    );
    return res.data;
  } catch (err) {
    console.log(err);
  }
}

export function sanitizeDescription(description: string) {
  description = description.replace(/^\/(\w+\s)/g, "\\/$1");
  description = description.replace(/\s\/(\w+\s)/g, " \\/$1");
  return description;
}
