import axios from "axios";

import config from "../config";

const api = axios.create({
  baseURL: config.imap.endpoint,
});

export async function setup() {
  try {
    await api.post("v1/settings", {
      webhooks: `${config.baseURL}/webhook`,
      webhookEvents: ["*"],
      notifyHeaders: ["*"],
      authServer: `${config.baseURL}/authentication`,
      notifyText: true,
      notifyTextSize: 0,
      logs: {
        all: false,
      },
    });

    await api.post("v1/account", {
      account: config.imap.accountID,
      name: config.imap.accountID,
      imap: {
        host: config.imap.host,
        port: config.imap.port,
        secure: config.imap.tls,
        useAuthServer: true,
        resyncDelay: 10,
      },
      smtp: {
        host: config.imap.host,
        port: config.imap.port,
        secure: config.imap.tls,
        useAuthServer: true,
      },
    });
  } catch (err) {
    throw new Error(`imap setup failed: ${err.message}`);
  }
}

export async function deleteMessage(id: string) {
  await api.delete(`v1/account/${config.imap.accountID}/message/${id}`);
}

export interface Message {
  id: string;
  date: string;
  unseen: boolean;
  subject: string;
  from: { name: string; address: string };
  attachments?: Attachment[];
  text: { plain: string };
}

export interface Attachment {
  id: string;
  contentType: string;
  encodedSize: number;
  filename: string;
}

export async function downloadAttachment(id: string): Promise<Buffer> {
  const response = await api.get(
    `v1/account/${config.imap.accountID}/attachment/${id}`,
    {
      responseType: "arraybuffer",
    }
  );

  return response.data;
}
